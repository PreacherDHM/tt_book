#pragma once

//Includs
#include <iostream>
#include <string.h>
#include <vector>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <fstream>

//Dice Types
#define D4 4
#define D6 6
#define D8 8
#define D10 10
#define D12 12
#define D20 20

//Player Class
#define PAGE_PLAYER	":PRCL:"
//Creacher
#define PAGE_CREACH	":CREA:"
//Creacher STATS
#define CREACH_HP		"[HP]"
#define CREACH_AC		"[AC]"
#define CREACH_MOV		"[MOV]"
#define CREACH_STR		"[STR]"
#define CREACH_DEX		"[DEX]"
#define CREACH_WIS		"[WIS]"
#define CREACH_INT		"[INT]"
#define CREACH_CRI		"[CRI]"
#define CREACH_CON		"[CON]"
#define CREACH_ATTACK	"[ATCK]"
//Vehical
#define PAGE_VEHI	":VEHI:"
#define VEHI_SPEED		"[SPED]"
#define VEHI_HP			"[HP]"
#define VEHI_AC			"[AC]"
//Item
#define PAGE_ITEM	":ITEM:"
//Weapon
#define PAGE_WEAP	":WEAP:"
#define WEAP_DAMAGE		"[DAMG]"
#define WEAP_DAMAGETYPE	"[TYPE]"
//Armmer
#define PAGE_ARMMER	":ARMM:"
#define ARMMER_TYPE		"[TPYE]"
#define ARMMER_AC		"[AC]"


//Block Types
#define TITLE			":T:"
#define DESCRIPTION		":D:"
#define BLOCK			":B:"

//Block Inserts
#define SPLIT	"[SPLIT]"
#define BOLD 	"*"
#define Attl	"*"
#define NL		"#"


//Macros
#define SelectType(data,Compare,DataType) if(strcmp(data,Compare) == 0) {return DataType;}
#define StrComp(data,Compare,Due) if(strcmp(data,Compare) == 0) {Due;}

namespace TT_Book {

	enum BlockType{
		Text,
		Number
	};
	struct Block {
		BlockType type;
		std::string data;
		virtual void SetData(const char* raw);
	};
	struct CreachBlock : public Block {

		int Ac;
		int Hp;
		int Mov;
		int Str;
		int Dex;
		int Wis;
		int Int;
		int Cri;
		int Con;

		std::string Attack;

		void SetData(const char* raw);
	};
	struct ItemBlock : public Block {
		void SetData(const char* raw);
	};

	struct AttackBlock : public Block {
		char** Attacks;
		void SetData(const char* raw);
	};

	struct List {
		const char* ListName;
		
	};

	//Book Objects

	enum PageType {
		Item,
		Weapon,
		Armmer,
		Creacher,
		PlayerClass,
		Vehical
	};
	class Page {
	private:
		PageType m_PageType;
		std::vector<Block*> m_PageBlocks;
		std::vector<List*> m_PageLists;
		std::string m_PagePath;

		void SetBlocks();
		void SetList();
		void SetListType(PageType pageType) {m_PageType = pageType;}

	public:
		Page(const char* FilePath){m_PagePath = FilePath;}

		const char* pageTitle;
		
		unsigned int GetBlockAmount()		{return m_PageBlocks.size();}
		PageType GetPageType()				{return m_PageType;}
		Block* GetBlock(unsigned int pos) 	{return m_PageBlocks[pos];}
		List* GetList(unsigned int pos)		{return m_PageLists[pos];}
		

	};

	class Section {
	private:
		std::vector<Page> m_pages;
		void CreatePages(const char* filePath);
	public:
		int GetPageCount();
		Page FindPage(const char* pageName);
	};

	class Book {
	private:
		std::vector<const char*> Sections;
		int PageCount = 0;

		const char* FindSection();
		Section GetSection();
		void SetPageCount();
	public:
		void InitBook(const char* path);
		Section FindSectons(const char* path);
	};



	//String exstracter
	class Exstracter {
	public:

		static const char* StringExstracter(const char* filePath);

		static const char** FormatExstracter(const char* page, unsigned int pageSize);
	private:
		std::vector<std::string> m_pageBlocks;
	};


	class Printer {
	public:
		//Book Printers :: Printers that Print out contents of the Book
		static void PrintPage(Page* page);
		static void printSection(Section* section);
		//Serch Printers :: Printers that Print out contents of a serch with in a book or section
		
	};

	//Math Commands
	class Command {
	private:
		static Block* SetBlockDataType(const char* blocktype);
	public:
		static int Calc_Stat(int stat)	{return (stat - 10)/2;}
		static int Role(int dt)			{srand(time(NULL)); return rand() % dt + 1;}

		static BlockType GetBlockType(const char* raw);
		static Block* CreateBlock(const char* blocktpye, const char* raw);
	
	};
}
